This is a small "nothing" package that is attempting to replicate an issue that
we've seen in [gentoo-haskell](https://github.com/gentoo-haskell/gentoo-haskell/).

Specifically: when the `USE="doc"` flag is enabled, *and* the test suite is run,
we get build failures if the package includes doctests.

For a demonstration, try this:

```sh
# this assumes package.use is a file...
echo "dev-haskell/machines doc" >> /etc/portage/package.use

# get the dependencies
emerge --onlydeps --with-test-deps =dev-haskell/machines-0.7.1

# I run ebuild directly to avoid FEATURES="test"...
cd $(portageq get_repo_path / haskell)/dev-haskell/machines
ebuild machines-0.7.1.ebuild test
```

The purpose of this small cabal package is to try to replicate this failure with
the most minimal code base possible, in order to better understand the issue and
resolve it.

We can reproduce the issue as follows:

```sh
ghc Setup.hs -o setup
./setup configure --enable-tests
./setup build
./setup test # no problems
./setup haddock
./setup test # tests fail
```

The failure happens due to the `./setup haddock` command deleting something in
the `./dist/package.conf.inplace` folder
