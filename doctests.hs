-- import Test.DocTest
-- main = doctest ["Main.hs"]

 module Main where

import Build_doctests (flags_exe_run_tests, pkgs_exe_run_tests, module_sources_exe_run_tests)
import Data.Foldable (traverse_)
import Test.DocTest

main :: IO ()
main = do
    traverse_ putStrLn args
    doctest args
  where
      args = flags_exe_run_tests ++ pkgs_exe_run_tests ++ module_sources_exe_run_tests
